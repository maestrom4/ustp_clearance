import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import {Http,Headers } from '@angular/http';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
 
  private _officeType;
  searchStd = '' ;
  myFormSearch: FormGroup;
  mySearchToggle2: FormGroup;
  private _listOffices = ["","","library","sawo","site","assessment","dean","chairman"];
  private _stdData = {
    id: 999999999,
    fname: "",
    mname:"",
    lname:"",
    course: "",
    year: 1,
    note: "dfsdfsfsf",
    office: "Library",
    clearance: true,
    visible: false,
    img: "assets/img/1gdF1VL4Q6CaNmJAog5m_rollouqi.png"

  };
  
  public selectedYear;
  public selectedSem;
  // public test = {
  //   id:234224,
  //   note: "Testing nga note",
  //   clearance: 1
  // };
  private _office = 2;
  public office =2;
  constructor(public auth: AuthProvider,public storage: Storage,public navparams: NavParams, public events:Events,public navCtrl: NavController, public http:Http,private builder: FormBuilder) {
    this.myFormSearch = this.builder.group({
      'searchStd':['']
    });
    this.mySearchToggle2 = this.builder.group({
      'clearanceToggle':['']
    });
    this.searchUser();
    // this.searchUser();
    // events.subscribe('admin:officeType',(officeType2)=>{
    //   console.log(officeType2);
    //   this._officeType = officeType2;
    // // this._stdData.office();
    // });
  }
  
  searchUser(){
    console.log(this.myFormSearch.value.searchStd);
    var link = 'http://ustpclearance.dev/user/student/search2/'+this.myFormSearch.value.searchStd;
    return new Promise((resolve,reject)=>{
      let headers = new Headers();
      // headers.append("","");
      this.http.get(link)
        .map(res=>res.json())
        .subscribe(data=>{
          console.log(data);
          
          console.log("///////office: "+resolve);
          console.log("+++++++++++++++++");
          console.log(this.getData('office'));
          // this._office = this.navparams.get('officeType2');
          // console.log("nav push data: "+this._office);
          // var temp2 = this._office;
          // var temp = this._listOffices[this._office];
          // console.log("tempdata: "+data.temp);
        
          // for(temp of this._listOffices){
          //   
          // }
          console.log("+++++++++++++++++");
          console.log(this._office);
          console.log("auth office data: "+this.auth.datax[0]);
          console.log("*******"+this._office);
          var x = this.officeToJson(data,this._office);
          console.log("test:"+x);
          console.log("experi ni:"+(x==1) ? 'true':'false');
          // 
          // console.log("from storagelocal: "+);
          this._stdData = {
            id:data.user_id,
            fname: data.fname,
            mname: data.mname,
            lname: data.lname,
            course: data.course,
            year: data.year,
            note: data.note,
            office: "LIbrary",
            clearance: (x!=0) ? true:false,
            visible: false,
            img: "assets/img/1gdF1VL4Q6CaNmJAog5m_rollouqi.png"
          }
        })
    })
  }
  officeToJson(data,office){
    
    var temp;
    if(office==2){
        temp = data.library;
    }
    if(office==3){
        temp = data.sawo;
    }
      if(office==4){
        temp = data.site;
    }
      if(office==5){
        temp = data.dean;
    }
      if(office==6){
        temp = data.chairman;
    }
    return temp;
  }
  setData(key,data){
    this.storage.set(key,data);
  }
  getData(key){
    this.storage.get(key).then((data2)=>{
      this._office = data2;
      console.log("data from localstorage: "+data2);
      console.log("Office data: "+this._office);
    });
  }
  searchClearedtoggled(){
    console.log("click toggle");
    var tempx = 0;
    var tempy = this.mySearchToggle2.value.clearanceToggle;
    console.log(tempy);
    // if(this.mySearchToggle2.value.clearanceToggle){
    //   tempx = 1
    // }else {
    //   tempx = 0;
    // }
    // var headers = new Headers();
    // headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json' );
    // // let options = new RequestOptions({ headers: headers });
    
    // this.http.post("http://ustpclearance.dev/user/student/updatec/"+this._stdData.id+"/"+this.office+"/"+tempy,{ headers:headers })
    //   .subscribe(data => {
    //     console.log(data);
    //    }, error => {
    //     console.log(error);// Error getting the data
    //   });
  }
  selectedSemFunc(){

    this.processSemYear();
  }
  selectedYearFunc(){
    this.processSemYear();
  }
  processSemYear(){
    console.log("selected sem: "+this.selectedSem);

    // this.resetCheckBox();
    var link = 'http://ustpclearance.dev/user/student/trans_semyearid/'+this._stdData.id +'/'+this.selectedSem+'/'+this.selectedYear;
    console.log("link: "+link);
    // var dataArray = [link];
    // return new Promise((resolve,reject)=>{
      let headers = new Headers();
      // console.log("student role:"+this.stdRole+"*********");
        this.http.get(link,{headers:headers})
          .map(res=>res.json())
          .subscribe(data=>{
            data.forEach((index,item)=>{
              console.log(data[item]);
              // console.log(data[item]);
              // if(data[item]['sng_id']==1){ //if student org
              //   console.log("student record!");
              //   // this._stdData.library=0;
              // }
          
            })
           
            
            // console.log(this.officeList);
           
        })
  }
  
}
