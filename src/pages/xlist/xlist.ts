import { Component } from '@angular/core';
import { NavController,Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Http,Headers,RequestOptions } from '@angular/http';
// import { Events } from 'ionic-angular';
@Component({
  selector: 'page-xlist',
  templateUrl: 'xlist.html'
})
export class XlistPage {
  public xlist:any;
  public selectedYear = 2017;
  public selectedSem = 1;
  public officeId:any;
  public officeName:any;
  constructor(public events:Events,public http:Http,public storage:Storage,public navCtrl: NavController) {
    
    this.storage.get("office").then((data)=>{
      console.log("office------------------>>>>>>>"+data[0]["sng_name"]);
      this.officeName = data[0]["sng_name"];
      this.officeId = data[0]["sng_id"];
    });
    this.events.subscribe("officeInfo",(offname, offid)=>{
      console.log("officename: "+offname);
      this.officeName = offname;
      this.officeId = offid;
    });
    this.xlist = new Array();
    // this.selectedSemFunc();
    // this.selectedYearFunc();
    this.searchXlist();
  } 
  searchXlist(){
    
    let link = 'http://ustpclearance.dev/user/admin/searchtransactiontbl/'+this.officeId+"/"+this.selectedSem+'/'+this.selectedYear;
        console.log(link);
        // return new Promise((resolve,reject)=>{
          let opt: RequestOptions;
          let myHeaders: Headers = new Headers;
          // myHeaders.set('app-id', 'c2549df0');
          // myHeaders.append('app-key', 'a2d31ce2ecb3c46739b7b0ebb1b45a8b');
          myHeaders.append('Content-type', 'application/json')
          opt = new RequestOptions({
            headers: myHeaders
          });
          // var data = JSON.stringify({username: "admin"});
          this.http.get(link, opt)
            .map(res=>res.json())
            .subscribe(data => {
              if(data.result){
                this.xlist = new Array();
                // console.log("length: "+data.query.length);
                // console.log("res!"+data);
                // console.log("result!"+data.result);
                // console.log("result!"+data.query[1].trans_id);
                data.query.forEach((item,index)=>{
                  // console.log("index: "+index);
                  // console.log("item: "+item);
                  this.xlist.push(item);
                });
                console.log("Xlist data: ");
                console.log(this.xlist);
              }else {
                this.xlist = [];
              }
              
            }, error => {
                console.log("Oooops!");
            });
     
  }
  selectedYearFunc(){
    this.selectedSemYear();
  }
  selectedSemFunc(){
    this.selectedSemYear();
  }
  selectedSemYear(){
    this.searchXlist();
  }
}
