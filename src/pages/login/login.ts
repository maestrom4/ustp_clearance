import { Component, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { AppModule } from '../../app/app.module';
import { NavController, AlertController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Platform, Nav, Events } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { SearchPage } from '../search/search';
import { StudentPage } from '../student/student';
// import {RestapiServiceProvider} from '../../providers/restapi-service/restapi-service';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
// import { AuthProvider } from '../../providers/auth/auth';
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  
})
export class LoginPage {
  public dataArrayAll= [];
  private submitted= false;
  fadeState: String = 'invisible';
  public errors = {
      errorCheck:false,
      message: "none"
  }
  splash = true;
  myForm: FormGroup;
  private myData: any;
  // loginData = { email:'', password:'' };
  constructor(public alertCtrl:AlertController, public http: Http,public storage: Storage,public events:Events,public navCtrl: NavController,private builder: FormBuilder) {
    this.myForm = this.builder.group({
      'idnum':['',Validators.compose([Validators.required])],
      'password':['',Validators.compose([Validators.required])],
    });
    this.errors = {
      errorCheck:false,
      message: "none"
    }
    this.storage.set("isLogged_in",[]);
    // try{
      
    //   this.storage.get("isLogged_in").then((data)=>{
    //     if(data==null){
    //       console.log("data: **********null**************");
    //       return
    //     }
    //     else {
    //       console.log("data: **********not null**************");
    //         if(data[1]!=1){
    //           this.events.publish('user:loginType',true,data[1],data[2],data[3],data[4]);
    //           this.navCtrl.setRoot(SearchPage);
    //         }else {
    //           this.events.publish('user:loginType',false,data[1],data[2],data[3],data[4]);
    //           this.navCtrl.setRoot(StudentPage);
    //         }
    //     }
        
    //   });
    // }catch(err){
    //      this.storage.set("isLogged_in",[]);
    // }
    //
    
    // this.storage = storage;
  }
  
  /**
   * Wala pa ni n gamit kay not working balikan nmo ni bai pg fix n ang uban
   */
  // toggleFade() {
  //   this.fadeState = (this.fadeState == 'visible') ? 'invisible' : 'visible';    
  // }
  logIn(usertype,userRole,id,officelist,officeinfo) {
    this.events.publish('user:loginType',usertype,userRole,id,officelist,officeinfo);
  }
  ionViewDidLoad() {
    setTimeout(() => {
      this.splash = false;
    },4000);
  }
  verifyUser2(){
    console.log("Fdfsfsfsf");
  }
  goToSignup(params){
    if (!params) params = {};
    this.navCtrl.push(SignupPage);
  }goToLogin(params){
    if (!params) params = {};
    this.navCtrl.push(LoginPage);
  }goToSearch(params){
    if (!params) params = {};
    this.navCtrl.push(SearchPage);
  }
  togglePane(){
    // console.log("current slide stats: ");
  }
  verifyUser(){
    // this.submitted = true;
    this.storage.clear();
    var idnum = this.myForm.value.idnum;
    var pass = this.myForm.value.password;
    let temp = "http://ustpclearance.dev/api";
    let apkey = "?X-API-KEY=ihTrtWPHSZ";
    var link = temp+'/student/login/'+idnum+'/'+pass+apkey;
   
    // return new Promise((resolve,reject)=>{
      let headers = new Headers();
      // var creds = "username=student &password=123456";
        this.http.get(link,{headers:headers})
          .map(res=>res.json())
          .subscribe(data=>{
            console.log("user role: "+data.roles_usr_role_id);
            console.log(data.user_role);
            // console.log("+++++++++++++++++++++++++++++++++++");
            if(!data.status){ //if status is false no record found return
              this.presentAlert("Does not Exist!", "No user found", "OK");
              return;
            }
            let tempOffice = new Array;
            // console.log(data);
            // console.log(data['officeslist']);
            // console.log(data['officeslist'][1]);
            let tempOfficeInfo = new Array;
            let tempArray:any;
            tempOfficeInfo = data['office'];
            // console.log("****************--------------");
            // console.log("tempOfficeInfo: "+tempOfficeInfo[0]['sng_id']);
            // console.log("tempOfficeInfo: "+tempOfficeInfo);
            tempArray = data['officeslist'];
            tempArray.forEach((row,index,element) => {
              // console.log("row: "+row['sng_name']);
              // console.log("index: "+index);
              tempOffice.push(row['sng_name']);
              // console.log("element: "+index);
            });
            
            
            if(data.status){
              // this.navCtrl.remove(0,2);
              console.log(data.roles_usr_role_id);
              if(data.roles_usr_role_id!=1){
                // console.log("*****idnum:");
                this.logIn(true,data.roles_usr_role_id,idnum,tempOffice,data['office']);
                // console.log("---sa admin----------- "+tempOffice);
                // this.navCtrl.push(SearchPage, {
                //   officeType2: data.role
                // }); pra e pasa nya ang type of office sa user
                // interefares with navigation
                // adds back button instead of menu
                // console.log("From login: "+data.role);
                this.setData("userType",true); //admin if true
                // console.log("*****usertype:");
                var dataPromise = this.storage.get("logged_in");
                this.storage.set("userType",true);
                // console.log(this.getData("userType"));
                this.setData("idnum",idnum);
                // console.log("idnum "+idnum);
                // console.log(this.getData("idnum"));
                this.setData("isLogged_in",[true, data.roles_usr_role_id, idnum, tempOffice,data['office']]);
                this.setData("office",data['office']);
                // console.log("loggedin------"+this.getData("logged_in"));
                // this.auth.datax = [data.role];
                // console.log("auth data: "+this.auth.datax[0]);
                // console.log("data stored for localstorage: ");
                // this.getData("office");
                this.navCtrl.setRoot(SearchPage);
                
              }else {
                // console.log("---sa student----------- "+tempOffice);
                this.logIn(false,1,idnum,tempOffice, data['office']);
                this.setData("userType",false); //student if false
                this.setData("userdata",data.roles_usr_role_id);
                this.setData("office",data.sign_office_sng_id);
                this.setData("idnum",idnum);
                this.setData("isLogged_in",[false,1, idnum, tempOffice,data['office']]);
                this.navCtrl.setRoot(StudentPage);
              }
              console.log("Success bai");
              idnum = null;
              pass = null;
            
            }else {
              console.log("Error bai");
              var temp = this;
              temp.errors.errorCheck = true;
              temp.errors.message="Check your username \n password";
              let x = setTimeout(function() {
                // this.getLoginerror();
                temp.errors.errorCheck = false;
                // temp.errors.message="Check your username \n password";
              }, 2000);
              // clearTimeout(x);
              
            }
          })
            this.storage.set("isLogged_in",true);
    // });
  }
  setData(key,data){
    this.storage.set(key,data);
  }
  getData(key){
    this.storage.get(key).then((data2)=>{
      // console.log("data from localstorage: "+data2);
      // return data2;
    });
  }
  isLoggedIn(){
    // this.submitted = true;
    let temp = "http://ustpclearance.dev/api";
    let apkey = "?X-API-KEY=ihTrtWPHSZ";
    var link = temp+'/student/sessionretreive'+apkey;
      let headers = new Headers();
        this.http.get(link,{headers:headers})
          .map(res=>res.json())
          .subscribe(data=>{
            console.log(data);
            return data;
          })
    // });
  }
  presentAlert(title,message, btnName) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [btnName]
    });
    alert.present();
  }
  // getLoginerror(){
  //   this.errors = {
  //     errorCheck:true,
  //     message: "Check your username \n password"
  //   }
  //   return this.errors;
  // }
 
}