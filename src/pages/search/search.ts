import { Component } from '@angular/core';
import { NavController,NavParams, AlertController } from 'ionic-angular';
import {Http,Headers,RequestOptions } from '@angular/http';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
 
  private _officeType;
  searchStd = '' ;
  myFormSearch: FormGroup;
  mySearchToggle2: FormGroup;
  public officeName="";
  public officeId="";
  public selectedSem;
  public selectedYear;
  public semdata: [1,2,3];
  public yearStart=1991;
  public yearArray=[];
  public yearBool=false;
  public cleared=false;
  public uncleared=false;
  public note = "";
  public clean = "";
  public toggleClearance=true;
  public fabEnable= false;
  private _listOffices = ["","","library","sawo","site","assessment","dean","chairman"];
  private _stdData = {
    id: 'xxxxxxxxxxxx',
    fname: "",
    mname:"",
    lname:"",
    course: "",
    year: 1,
    note: "dfsdfsfsf",
    office: "Library",
    clearance: true,
    visible: false,
    img: "assets/img/1gdF1VL4Q6CaNmJAog5m_rollouqi.png"

  };
  

  // public test = {
  //   id:234224,
  //   note: "Testing nga note",
  //   clearance: 1
  // };
  private _office = 2;
  public office =2;
  constructor(public alertCtrl: AlertController,public auth: AuthProvider,public storage: Storage,public navparams: NavParams, public events:Events,public navCtrl: NavController, public http:Http,private builder: FormBuilder) {
    this.myFormSearch = this.builder.group({
      'searchStd':['']
    });
    this.fabEnable = true;
    this.clean = "Clean record";
    this.clearedUnclearedfld(true);
    this.mySearchToggle2 = this.builder.group({
      'clearanceToggle':['']
    });
    this.yearBool = false;
    let i: number;
    this.selectedSem = 1;
    this.selectedYear = 2017;
    for(i=this.yearStart; i<=2017;i++){
      // console.log(i);
      this.yearArray.push(i);
    }
    // this.searchUser();
    // this.events.subscribe('userloggedin:data', (data)=>{
    //   console.log("userdata------------------>>>>>>>"+data[0]["sng_name"]);
    // });
    // this.searchUser();
    // events.subscribe('admin:officeType',(officeType2)=>{
    //   console.log(officeType2);
    //   this._officeType = officeType2;
    // // this._stdData.office();
    // });
    this.storage.get("office").then((data)=>{
      console.log("office------------------>>>>>>>"+data[0]["sng_name"]);
      this.officeName = data[0]["sng_name"];
      this.officeId = data[0]["sng_id"];
      // this.events.publish("officeInfo", data[0]["sng_id"],data[0]["sng_name"]);
    });
  }
  
  searchUser(){
    console.log(this.myFormSearch.value.searchStd);
    if (this.myFormSearch.value.searchStd==""){
      this.presentAlert("Input blank!", "search ID is blank", "");
      return;
    }
    var link = 'http://ustpclearance.dev/user/admin/searchidsemyearoffs/'+this.myFormSearch.value.searchStd+'/'+this.selectedSem+'/'+this.selectedYear+"/"+this.officeId+"/";
    console.log(link);
    return new Promise((resolve,reject)=>{
 
 
      let opt: RequestOptions;
      let myHeaders: Headers = new Headers;
      // myHeaders.set('app-id', 'c2549df0');
      // myHeaders.append('app-key', 'a2d31ce2ecb3c46739b7b0ebb1b45a8b');
      // myHeaders.append('Content-type', 'application/json')
      
      opt = new RequestOptions({
        headers: myHeaders
      }) 
      this.http.get(link, opt)
        .map(res=>res.json())
        .subscribe(data=>{
          //If student is in the transaction database. Students with past records
          console.log(data);
          console.log(data.query);
          console.log((data.trans_status==1)?true:false);
          // var x = this.officeToJson(data,this._office);
          if(data.result){
            this.clean = "With record";
            this._stdData = {
              id:data.query.usr_acc_id,
              fname: data.query.usr_fname,
              mname: data.query.usr_mname,
              lname: data.query.usr_lname,
              course: data.query.crs_name,
              year: data.query.std_year,
              note: data.query.usr_note,
              office: this.officeName,
              clearance: (data.query.trans_status==1)?true:false,
              visible: false,
              img: "assets/img/1gdF1VL4Q6CaNmJAog5m_rollouqi.png"
            }
            this.clearedUnclearedfld((data.query.trans_status==1)?true:false);
             this.fabEnable =false;
          }else {
            this.clean = "Clean record";
            this.presentAlert("Clean record", "Cleared  for this sem", "OK");
            this._stdData = {
              id:this.myFormSearch.value.searchStd,
              fname: data.query.usr_fname,
              mname: data.query.usr_mname,
              lname: data.query.usr_lname,
              course: data.query.crs_name,
              year: data.query.std_year,
              note: "*****Clean record for this sem******",
              office: this.officeName,
              clearance: true,
              visible: false,
              img: "assets/img/1gdF1VL4Q6CaNmJAog5m_rollouqi.png"
           
            }
            this._stdData.clearance = true;
            this.clearedUnclearedfld(true);
            this.fabEnable = true;
          }
          
        })
    })
  }

  setData(key,data){
    this.storage.set(key,data);
  }
  getData(key){
    this.storage.get(key).then((data2)=>{
      this._office = data2;
      console.log("data from localstorage: "+data2);
      console.log("Office data: "+this._office);
    });
  }
  clearedUnclearedfld(tempval){
    if(tempval){
      this.cleared = true;
      this.uncleared=false;
    }else {
      this.cleared=false;
      this.uncleared=true;
      
    }
  }

  searchClearedtoggled(){
    
    console.log("click toggle");
    console.log("toggle value: "+this.toggleClearance);
    var tempx = 0;
    var tempy = this.mySearchToggle2.value.clearanceToggle;
    this.clearedUnclearedfld(tempy);
    console.log(tempy);
    let statusValue = 0;
    if(this.toggleClearance){
      // statusValue=1;
      console.log("------"+this._stdData.note);
      this.searchClearedtoggleExtension(1, null);
    }else {
      let noted = "";
      this.presentAlertAddNote(statusValue);
    }      
  }
  searchClearedtoggleExtension(statusValue, noted){
    this._stdData.note = noted;
    let link = 'http://ustpclearance.dev/user/admin/updatetransstatus/'+this.myFormSearch.value.searchStd+'/'+this.selectedSem+'/'+this.selectedYear+"/"+this.officeId+"/"+statusValue+"/"+noted;
      console.log(link);
      // return new Promise((resolve,reject)=>{
        let opt: RequestOptions;
        let myHeaders: Headers = new Headers;
        // myHeaders.set('app-id', 'c2549df0');
        // myHeaders.append('app-key', 'a2d31ce2ecb3c46739b7b0ebb1b45a8b');
        myHeaders.append('Content-type', 'application/json')
        opt = new RequestOptions({
          headers: myHeaders
        });
        var data = JSON.stringify({username: "admin"});
        this.http.get(link, opt)
          .subscribe(data => {
              console.log("res!"+data);
          }, error => {
              console.log("Oooops!");
          });
  }
  selectedSemFunc(){

    this.processSemYear();
  }
  selectedYearFunc(){
    this.processSemYear();
  }
  processSemYear(){
    this.searchUser();
    // console.log("selected sem: "+this.selectedSem);
  }
  presentAlert(title,message, btnName) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [btnName]
    });
    alert.present();
  }

  presentAlertAddNote(statusValue) {
  let note = "";

    const alert = this.alertCtrl.create({
      title: 'Enter note',
      inputs: [
        {
          name: 'note',
          placeholder: 'Note'
        }
      
      ],
      buttons: [

        {
          text: 'Submit',
          handler: data => {
            if(data.note==""){
              this.presentAlert("Blank", "Blank not allowed", "OK");
              this.clearedUnclearedfld(true);
              this._stdData.clearance = true;
              return;
            }else {
              this.searchClearedtoggleExtension(statusValue,data.note);
              // resolve(data.note);
              // resolve(data.note);
            }
            // if (User.isValid(data.username, data.password)) {
            //   // logged in!
            // } else {
            //   // invalid login
            //   return false;
            // }
          }
        }
      ]
    });
  alert.present();

  }
  addTransactions(noted){
    let link = 'http://ustpclearance.dev/user/admin/addtransactions/'+this.myFormSearch.value.searchStd+'/'+this.selectedSem+'/'+this.selectedYear+"/"+this.officeId+"/"+noted;
        console.log(link);
        // return new Promise((resolve,reject)=>{
          let opt: RequestOptions;
          let myHeaders: Headers = new Headers;
          // myHeaders.set('app-id', 'c2549df0');
          // myHeaders.append('app-key', 'a2d31ce2ecb3c46739b7b0ebb1b45a8b');
          myHeaders.append('Content-type', 'application/json')
          opt = new RequestOptions({
            headers: myHeaders
          });
          var data = JSON.stringify({username: "admin"});
          this.http.get(link, opt)
            .subscribe(data => {
                console.log("res!"+data);
            }, error => {
                console.log("Oooops!");
            });
  }
  addTransactionBtnCtrl(){
    if (this.myFormSearch.value.searchStd==""){
      this.presentAlert("Input blank!", "search ID is blank", "");
      return;
    }else {
      this.presentAlertAddNote2();
    }
  }
  presentAlertAddNote2() {
  
  console.log("button pressed!");
  let note = "";

    const alert = this.alertCtrl.create({
      title: 'Enter note',
      inputs: [
        {
          name: 'note',
          placeholder: 'Note'
        }
      
      ],
      buttons: [

        {
          text: 'Submit',
          handler: data => {
            if(data.note==""){
              this.presentAlert("Blank", "Blank not allowed", "OK");
              this.clearedUnclearedfld(true);
              this._stdData.clearance = true;
              return;
            }else {
              this.addTransactions(data.note);
              // resolve(data.note);
              // resolve(data.note);
            }
            // if (User.isValid(data.username, data.password)) {
            //   // logged in!
            // } else {
            //   // invalid login
            //   return false;
            // }
          }
        }
      ]
    });
  alert.present();

  }        
}
