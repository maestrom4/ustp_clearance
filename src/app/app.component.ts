import { Component, ViewChild } from '@angular/core';
import { Platform, Nav,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http,Headers } from '@angular/http';
import { XlistPage } from '../pages/xlist/xlist';
import { AddUpdateStudentPage } from '../pages/add-update-student/add-update-student';
import { StudentPage } from '../pages/student/student';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { Storage } from '@ionic/storage';


import { SearchPage } from '../pages/search/search';



@Component({
  templateUrl: 'app.html'
  // template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = LoginPage;
  public admin:any;
  public student:any;
  public userRole:number;
  public userId: any;
  public officeList:any;
  public officeInfo:any;
  constructor(public http: Http, public storage: Storage,public events: Events,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      /**
       * Usertype if admin or student
       */
      this.admin = false;
      this.student = false;
      this.events.subscribe('user:loginType',(usertype,userrole,id,officelist, officeinfo)=>{
        // console.log("*********************");
        // console.log("result: "+usertype);
        if(usertype==false){
          this.student = true;
        }else {
          this.admin = true;
        }
        this.officeList = officelist;
        this.storage.set('officeList', officelist);
        // console.log("********this.student*************: "+this.student);
        // console.log("********this.admin*************: "+this.admin);
        // console.log("**********Components***********");
        this.userRole = userrole;
        this.userId = id;
        this.officeInfo = officeinfo; //needs key to access data used array first then key example officeinfo[0]['sng_id']
        console.log("**********officeinfo***********"+officeinfo[0]['sng_name']);
        // this.events.publish('userloggedin:data', officeinfo);
       
      });
    });
  }
  logout(){
    this.storage.clear();

    this.admin = false;
    this.student = false;
    // console.log("********Logout this.student*************: "+this.student);
    // console.log("********Logout this.admin*************: "+this.admin);
    // console.log("LOgin button click!");
    // http://ustpclearance.dev/api/student/logout?X-API-KEY=ihTrtWPHSZ
    // var link = 'http://ustpclearance.dev/user/student/logout/';
    let temp = "http://ustpclearance.dev/api";
    let apkey = "?X-API-KEY=ihTrtWPHSZ";
    var link = temp+'/student/logout'+apkey;
    // return new Promise((resolve,reject)=>{
      let headers = new Headers();
      this.http.get(link,{headers:headers})
        // .map(res=>res.json())
        .subscribe(data=>{
          // console.log("services: "+data.status);
      
        })
     
      this.navCtrl.setRoot(LoginPage);
  }
  goToSearch(params){
    if (!params) params = {};
    this.navCtrl.setRoot(SearchPage);
  }goToXlist(params){
    if (!params) params = {};
    this.navCtrl.setRoot(XlistPage);
  }goToAddUpdateStudent(params){
    if (!params) params = {};
    this.navCtrl.setRoot(AddUpdateStudentPage);
  }goToStudent(params){
    if (!params) params = {};
    this.navCtrl.setRoot(StudentPage);
  }goToLogin(params){
    if (!params) params = {};
    this.navCtrl.setRoot(LoginPage);
  }goToSignup(params){
    if (!params) params = {};
    this.navCtrl.setRoot(SignupPage);
  }
}
