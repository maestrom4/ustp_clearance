import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SearchPage } from '../pages/search/search';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { XlistPage } from '../pages/xlist/xlist';
import { AddUpdateStudentPage } from '../pages/add-update-student/add-update-student';
import { StudentPage } from '../pages/student/student';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { StdClearanceProvider } from '../providers/std-clearance/std-clearance';

@NgModule({
  declarations: [
    MyApp,
    SearchPage,
    LoginPage,
    SignupPage,
    XlistPage,
    AddUpdateStudentPage,
    StudentPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SearchPage,
    LoginPage,
    SignupPage,
    XlistPage,
    AddUpdateStudentPage,
    StudentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    StdClearanceProvider
  ]
})
export class AppModule {}