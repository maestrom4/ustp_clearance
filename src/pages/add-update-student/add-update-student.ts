import { Component } from '@angular/core';
import { NavController, AlertController,NavParams, LoadingController, Platform  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http,Headers,RequestOptions } from '@angular/http';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import 'rxjs/add/operator/map';
@Component({
  selector: 'page-add-update-student',
  templateUrl: 'add-update-student.html'
})
export class AddUpdateStudentPage {
  public btnName:any;
  public toggleValue:any;
  public updateAddDate = {
    'id':'',
    'fname':'',
    'mname':'',
    'lname':'',
    'course':'',
    'year':''
  }
  public userData:any;
  public orgs=[];
  public addBtn:boolean;
  public updateBtn:boolean;
  public courseSelected:any;
  public orgSelected: any;
  public courses=[];
  public yearSelected:any;
  public checkUser=false;
  addStudentForm: FormGroup;
  updateStudentForm: FormGroup;
  constructor(public formBuilder: FormBuilder,public alertctrl: AlertController, public http:Http,public navCtrl: NavController) {
    this.btnName = "Add";
    this.addBtn = true;
    this.updateBtn = false;
    this.toggleValue = true;
    this.courses = [];
    this.courseSelected="";
    this.orgSelected="";
    this.yearSelected="";
    // this.userData = null;
   this.getCoursesList();
   this.getorgList();
    // console.log("value: "+this.courses());
    this.addStudentForm = formBuilder.group({
      addUserid: ['', Validators.compose([Validators.required, Validators.maxLength(15)])], 
      addUserfname: ['', Validators.compose([Validators.required, Validators.maxLength(30)])], 
      addUsermname: ['', Validators.compose([Validators.required, Validators.maxLength(30)])], 
      addUserlname: ['', Validators.compose([Validators.required, Validators.maxLength(30)])], 
      addUsercourse: ['', Validators.compose([Validators.required])], 
      addUserorg: ['', Validators.compose([Validators.required])], 
      addUseryear: ['', Validators.compose([Validators.required])], 

    });
  }
  addUpdateStudentp(){
    console.log("button click : "+this.toggleValue);
    if(this.toggleValue){
      this.btnName = "Add";
      this.addBtn =true;
      this.updateBtn = false;
      this.addStudent();
      
    }else {
      this.btnName = "Update";
      this.updateStudent();
      this.addBtn =false;
      this.updateBtn = true;
      // this.getCoursesList();
      // this.getorgList();
    }
  }
  addStudent(){
    console.log("add student");
    console.log(this.updateAddDate.id);
    console.log(this.updateAddDate.fname);
    console.log(this.updateAddDate.mname);
    console.log(this.updateAddDate.lname);
    console.log(this.updateAddDate.course);
    console.log(this.updateAddDate.year);
    this.addStudentquery();
    // this.clearInputs();
  }
  updateStudent(){
    console.log("update student");

    console.log(this.updateAddDate.id);
    console.log(this.updateAddDate.fname);
    console.log(this.updateAddDate.mname);
    console.log(this.updateAddDate.lname);
    console.log(this.updateAddDate.course);
    console.log(this.updateAddDate.year);
    // this.updateStudentReader();
    // this.clearInputs();
  }
  updateStudentSearch(){
    let link = "http://ustpclearance.dev/user/admin/updatesearch/"+this.updateAddDate.id;
      // return new Promise((resolve,reject)=>{
        let data = this.http.get(link);
        let temp2 = [];
        data
        .map(res => res.json())
        .subscribe(data => {
          console.log('my data: ', data);
          // console.log(data);
          // data.forEach((item,index)=>{
          // temp2.push(item.crs_name);
          // this.courses.push(item.crs_name);
            // console.log(this.courses+" -"+index);
          }
        );
        // resolve(this.courses=temp2);
        //  console.log(temp2+" -list");
      // });
  }
  updateStudentbtn(){
    console.log("Update CLick");
  }
  getCoursesList(){
    let link = "http://ustpclearance.dev/user/admin/courses";
      // return new Promise((resolve,reject)=>{
        let data = this.http.get(link);
        let temp2 = [];
        data
        .map(res => res.json())
        .subscribe(data => {
          // console.log('my data: ', data);
          // console.log(data);
          data.forEach((item,index)=>{
          temp2.push(item.crs_name);
          this.courses.push(item.crs_name);
            // console.log(this.courses+" -"+index);
          }
          );
        // resolve(this.courses=temp2);
        //  console.log(temp2+" -list");
        });
        // return temp2;
      // });
      // this.courses = temp2;
      // console.log(this.courses);
  }
    getorgList(){
    let link = "http://ustpclearance.dev/user/admin/org/";
      // return new Promise((resolve,reject)=>{
        let data = this.http.get(link);
        let temp2 = [];
        data
        .map(res => res.json())
        .subscribe(data => {
          // console.log('my data: ', data);
          // console.log(data);
          data.forEach((item,index)=>{
            // console.log(index);
            if(index>5){
              temp2.push(item.orgName);
              this.orgs.push(item.sng_name);
            }
            
            // console.log(this.courses+" -"+index);
          }
          );
        // resolve(this.courses=temp2);
        //  console.log(temp2+" -list");
        });
        // return temp2;
      // });
      // this.courses = temp2;
      // console.log(this.courses);
  }
  courseSelectedFunc(){
    console.log(this.courseSelected+" : ");
  }
  orgSelectedFunc(){
    console.log(this.orgSelected+" : ");
  }
  addStudentquery(){
    // let tempy:any;
    if(!this.addStudentForm.controls.addUserid.valid){
      this.presentAlert("Don't leave blanks","Don't leave blanks","OK");
      return;
    }
    if(!this.addStudentForm.controls.addUserfname.valid){
      this.presentAlert("Don't leave blanks ","Don't leave Firstname blank","OK");
      return;
    }
    if(!this.addStudentForm.controls.addUserlname.valid){
      this.presentAlert("Don't leave blanks","Don't leave Lastname blank","OK");
      return;
    }
    if(!this.addStudentForm.controls.addUsermname.valid){
      this.presentAlert("Don't leave blanks","Don't leave Middlename blank","OK");
      return;
    }
    if(!this.addStudentForm.controls.addUsercourse.valid){
      this.presentAlert("Don't leave blanks","Don't leave Course blank","OK");
      return;
    }
    if(!this.addStudentForm.controls.addUseryear.valid){
      this.presentAlert("Don't leave blanks","Don't leave Year blank","OK");
      return;
    }
    // if(!this.addStudentForm.controls.addUserorg.valid){
    //   this.presentAlert("Don't leave blanks","Don't leave Org blank","OK");
    //   return;
    // }
      console.log("pass here!"); 

    this.checkuserId(this.updateAddDate.id);
    
    
    
      
    
      
  }
  clearInputs(){

    this.updateAddDate.id = "";
    this.updateAddDate.fname = "";
    this.updateAddDate.mname= "";
    this.updateAddDate.lname= "";
    this.userData = null;
  }
   presentAlert(title,message, btnName) {
    const alert = this.alertctrl.create({
      title: title,
      subTitle: message,
      buttons: [btnName]
    });
    alert.present();
  }
  checkuserId(id){
    let link = 'http://ustpclearance.dev/user/admin/search2/'+id;
    console.log(link);
    let data = this.http.get(link);
        let temp2 = [];
        data
        .map(res => res.json())
        .subscribe(data2 => {
          console.log('my data2: ', data2);
          // console.log(data2['0'].usr_acc_id); 
          console.log(data2['no_users']); 
          let x = [];
          x.push(data2['no_users']);
          console.log("****FINISH**//////////////***"); 
          
          if(data2['no_users']){
            // x.push(data2['0'].usr_acc_id);
            // x.push(data2['0'].usr_acc_pass);
            // x.push(data2['0'].usr_status);
            // x.push(data2['0'].usr_role_office_id);
            // x.push(data2['0'].usr_timestamp);
           this.addUserContinuation(x);
          }else {
               this.presentAlert("Already exist","Already exist","OK");
          }
        }
    );
  
   
  }
  public addUserContinuation(userData){
    console.log("before if: ");
    console.log(userData+"<<<<<<<<<<<<<<------------");

      
      let id = this.addStudentForm.controls.addUserid.value;
      console.log(id+": id");
      let link = 'http://ustpclearance.dev/user/admin/addstudent/'+this.updateAddDate.id+'/'+this.updateAddDate.fname+'/'+this.updateAddDate.mname+"/"+this.updateAddDate.lname+"/"+this.courseSelected+"/"+this.yearSelected;
      
        console.log(link);
        // return new Promise((resolve,reject)=>{
          let opt: RequestOptions;
          let myHeaders: Headers = new Headers;
          // myHeaders.set('app-id', 'c2549df0');
          // myHeaders.append('app-key', 'a2d31ce2ecb3c46739b7b0ebb1b45a8b');
          myHeaders.append('Content-type', 'application/json');
          opt = new RequestOptions({
            headers: myHeaders
          });
          var data = JSON.stringify({username: "admin"});
          this.http.get(link, opt)
            .subscribe(data => {
                console.log("res!"+data);
                
                this.presentAlert("Success", "Successfully added student", "Ok");
                this.clearInputs();
            }, error => {
                console.log("Oooops!");
            }
        );
      }
}
