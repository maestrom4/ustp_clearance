import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http,Headers } from '@angular/http';
import { StdClearanceProvider } from '../../providers/std-clearance/std-clearance';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-student',
  templateUrl: 'student.html'
})
export class StudentPage {
  public _toggleController:any;
  private _stdId:any;
  public selectedYear;
  public officeList:any;
  public selectedSem;
  public semdata: [1,2,3];
  public yearStart=1991;
  public yearArray=[];
  public yearBool:boolean;

  public stdRole:any;
  private _stdData={
    'library':1,
    'sawo': 1,
    'org': 1,
    'assessement':1,
    'dean':1,
    'chairman':1
  };
  public data:any;
  public link:any;
  // public selectedYear:any;
  constructor(public http:Http,private stduentCopy: StdClearanceProvider,public storage: Storage,public navCtrl: NavController) {
    this.yearBool = false;
    let i: number;
    this.selectedSem = 1;
    this.selectedYear = 2017;
    for(i=this.yearStart; i<=2017;i++){
      // console.log(i);
      this.yearArray.push(i);
    }
    console.log(this.yearArray);
    this.storage.get("idnum").then((res)=>{
      console.log("ID#: "+res);
      this._stdId = res;
      this.getStudentdata(res);
    //   // console.log(this.stduentCopy.allDatastudent);
    //   // console.log("-----------------");
    });
    this.processSemYear();
    
    
  }
  getStudentdata(res){
    this.officeList = new Array();
    this.storage.get('userdata').then((data)=>{
      this.stdRole = data.role;
    });
    let temp = "http://ustpclearance.dev/api";
    let apkey = "?X-API-KEY=ihTrtWPHSZ";
    console.log(this.selectedSem);
    console.log(this.selectedYear);
    this.link = temp+"/student/search2/"+res+'/'+this.selectedSem+'/'+this.selectedYear+apkey;
      this.data = this.http.get(this.link);
      this.data
      .map(res => res.json())
      .subscribe(data => {
        console.log('my data: ', data);
        this.storage.get("office");
        this._stdData={
          'library':data.library,
          'sawo': data.sawo,
          'org': data.org,
          'assessement':data.assessement,
          'dean':data.dean,
          'chairman':data.chairman
        };
      })
      this.processSemYear();
   
  }
  toggleDisable(){
    console.log("toggle disabled: ");
    this._toggleController="true";
  }
  selectedYearFunc(){
    // console.log("selected sem: "+this.selectedSem);
    //  console.log("************t "+this.selectedYear);
     this.processSemYear();
     
  }
  selectedSemFunc(){
    // console.log("selected sem: "+this.selectedSem);
    // console.log("************t "+this.selectedYear);
    this.processSemYear();
  }
  resetCheckBox(){

    this._stdData.library=1;
    this._stdData.sawo=1;
    this._stdData.org=1;
    this._stdData.assessement=1;
    this._stdData.chairman=1;
    this._stdData.dean=1;
  }
  processSemYear(){
    console.log("selected sem: "+this.selectedSem);

    this.resetCheckBox();
    let temp = "http://ustpclearance.dev/api";
    let apkey = "?X-API-KEY=ihTrtWPHSZ";
    var link = temp+'/student/trans_semyearid/'+this._stdId +'/'+this.selectedSem+'/'+this.selectedYear+apkey;
    console.log("link: "+link);
    // var dataArray = [link];
    // return new Promise((resolve,reject)=>{
      let headers = new Headers();
      // console.log("student role:"+this.stdRole+"*********");
        this.http.get(link,{headers:headers})
          .map(res=>res.json())
          .subscribe(data=>{
            // console.log("*********-------------------------");
            // console.log(data);
            for(let i in data){
              console.log(data[i]);
              // console.log(data[item]);
              if(data[i]['sign_office_sng_id']==1){ //if student org
                console.log("student record!");
                this._stdData.library=0;
              }
              if(data[i]['sign_office_sng_id']==2){ //if student org
                console.log("student record!");
                this._stdData.sawo=0;
              }
              if(data[i]['sign_office_sng_id']==3){ //if student org
                console.log("student record!");
                this._stdData.org=0;
              }
              if(data[i]['sign_office_sng_id']==4){ //if student org
                console.log("student record!");
                this._stdData.assessement=0;
              }
              if(data[i]['sign_office_sng_id']==5){ //if student org
                console.log("student record!");
                this._stdData.chairman=0;
              }
              if(data[i]['sign_office_sng_id']==6){ //if student org
                console.log("student record!");
                this._stdData.dean=0;
              }
            }
     
            console.log(this.officeList);
        })
  }
  
}
